import cv2
import numpy as np
import time

def templateMatch(im, tpl):
    img = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    H,W = img.shape
    template = cv2.cvtColor(tpl, cv2.COLOR_BGR2GRAY)
    h,w = template.shape
    method = cv2.TM_CCORR_NORMED

    # Apply template Matching
    res = cv2.matchTemplate(img,template,method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    end = time.time()
    if(max_val >= 0.95):
        return True
    return False

def sample_video(video_filename, num_of_samples):
    video = cv2.VideoCapture(video_filename)
    tot_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    samples = []
    k = 0; f = 1
    while video.isOpened():
        ret, frame = video.read()
        if ret and k % int(tot_frames/num_of_samples) == 0 and f <= num_of_samples:
            samples.append(frame)
            f+=1
        if(cv2.waitKey(1) & 0xFF == ord('q') or not ret):
            break
        k+=1
    return samples

if __name__ == "__main__":
    video_test = '' # QUI INSERIRE PATH VIDEO CROPPATO
    videos_in_dataset = [''] # QUI CREARE UNA LISTA DI PATH DEI VIDEO DEL DATASET. GUARDA METODO PYTHON glob()
    num_of_samples = 10
    index_of_corrispondence = -1

    samples_test = sample_video(video_test, num_of_samples)

    start = time.time()

    for j in range(len(videos_in_dataset)):
        video_dataset = videos_in_dataset[j]
        samples_dataset = sample_video(video_dataset, num_of_samples)

        if len(samples_test) > 0 and len(samples_test) == len(samples_dataset):
            they_match = True

            for i in range(len(samples_test)):
                if not templateMatch(samples_dataset[i], samples_test[i]):
                    they_match = False
                    break
  
        if they_match:
            index_of_corrispondence = j
            break
        else:
            continue

    end = time.time()

    print("Evaluation took "+str(end-start)+" seconds")
    if index_of_corrispondence >= 0:
        print("Match found in video: "+videos_in_dataset[index_of_corrispondence]+" of the dataset")
        
    else:
        print("No video in the dataset could match the cropped video")